start:
	ng serve

generate:
	ng build --configuration production

pwa:
	ng build --configuration production && http-server -p 8080 -c-1 dist/life
