export interface ActivityApiResult {
  'hydra:member': Activity[];
}

export interface Activity {
  id: number;
  name: string;
  description: string;
  start: string;
  end: string;
  albums?: [{title: string, shareableUrl: string, productUrl: string}];
  type?: {slug: string};
  users?: [{name: string, surname: string}];
}
