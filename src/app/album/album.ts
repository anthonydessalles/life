export interface AlbumApiResult {
  'hydra:member': Album[];
}

export interface Album {
  title: string;
  shareableUrl: string;
  productUrl: string;
}
