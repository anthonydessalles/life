export interface FileApiResult {
  'hydra:member': File[];
}

export interface File {
  filename: string;
  description: string;
  embedHtml: string;
}
