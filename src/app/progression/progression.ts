export interface ProgressionApiResult {
  'hydra:member': Progression[];
}

export interface Progression {
  name: string;
  description: string;
  difficulty: string;
  file?: {filename: string, path: string, source: string};
  type?: {slug: string};
  game?: {name: string};
  users?: [{name: string, surname: string}];
  videos?: [{position?:number, filename: string, embedHtml: string}];
}
