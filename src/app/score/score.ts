export interface ScoreApiResult {
  'hydra:member': Score[];
}

export interface Score {
  points: number;
  isHome: boolean;
  isDraw: boolean;
  isLose: boolean;
  isWin: boolean;
  score?: {difficulty?: string, home?: string, homePlayers?: number, homePoints?: number, away?: string, awayPlayers?: number, awayPoints?: number, duration?: number, game?: {name: string}, activity?: {name: string, start: string}, type?: {slug: string}};
}
