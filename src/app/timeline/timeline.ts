export interface TimelineApiResult {
  'hydra:member': Timeline[];
}

export interface Timeline {
  name: string;
  start: string;
  end: string;
}
