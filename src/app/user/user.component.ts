import { Component, OnInit, Inject } from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { DatePipe } from '@angular/common';
import { ChartData, ChartEvent, ChartType } from 'chart.js';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgbCarouselConfig, NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { NgFor, NgIf } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

import { Activity } from '../activity/activity';
import { Album } from '../album/album';
import { File } from '../file/file';
import { Progression } from '../progression/progression';
import { Score } from '../score/score';
import { Timeline } from '../timeline/timeline';
import { User } from './user';
import { UserService } from './user.service';
import { UserTokenSignin } from './userTokenSignin';

interface Badges {
   [key: string]: boolean;
}

export interface DialogData {
  slides: 0,
  youtubeEmbedId: '',
  id: 0,
}

@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
  constructor(private domSanitizer: DomSanitizer) {}
  transform(url: string) {
    return this.domSanitizer.bypassSecurityTrustResourceUrl(url);
  }
} 

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  providers: [UserService],
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  activity: Activity;
  activities = 'Activities';
  albums = 'Albums';
  events: string[] = [];
  files = 'Files';
  scores = 'Scores';
  loadingActivities: boolean;
  loadingAlbums: boolean
  loadingFiles: boolean
  loadingProgressions: boolean;
  loadingScores: boolean;
  loadingTimelines: boolean;
  progressions = 'Progressions';
  startDate: Date;
  timelines = 'Timelines';
  user: User;
  userActivities: Activity[] = [];
  userAlbums: Album[] = [];
  userData: UserTokenSignin;
  userFiles: File[] = [];
  userProgressions: Progression[] = [];
  userScores: Score[] = [];
  userTimelines: Timeline[] = [];

  // Doughnut
  public doughnutChartLabels: string[] = [];
  public doughnutChartData: ChartData<'doughnut'> = {
    labels: this.doughnutChartLabels,
    datasets: [
      { data: [] }
    ]
  };
  public doughnutChartType: ChartType = 'doughnut';
  public userBadges:Badges = {};

  constructor(
    private userService: UserService,
    public datePipe: DatePipe,
    public dialog: MatDialog
  ) {
    this.user = {
      name: '',
      surname: '',
      gamername: '',
      email: '',
      youtubeEmbedId: '',
      slides: 0,
      id: 0,
    };
    this.userBadges = {
      '10+': false,
      'football': false,
      'video_game': false,
      'nightclub': false,
      'contributor': false,
    }
  }

  ngOnInit() {
    this.userService.getUsername(String(localStorage.getItem('token')))
      .subscribe((data) => {
        this.userData = data;

        this.userService.getUser(this.userData.user, this.userData.token)
          .subscribe(data => {
            this.user = data;
            
            if (this.user.youtubeEmbedId && this.user.slides) {
              this.userBadges['10+'] = true;
            }

            this.user.userKpis?.forEach((value) => {
              // Badges
              if ('football' == value.type.slug) {
                this.userBadges[value.type.slug] = true;
              }
              if ('video_game' == value.type.slug) {
                this.userBadges[value.type.slug] = true;
              }
              if ('nightclub' == value.type.slug) {
                this.userBadges[value.type.slug] = true;
              }
              if (this.user.gamername) {
                this.userBadges['contributor'] = true;
              }

              // Chart
              if ('activity' == value.kpiType) {
                this.doughnutChartLabels.push(value.type.slug);
                this.doughnutChartData.datasets[0].data.push(value.total);
              }
            });
          });
      });
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.events.push(`${type}: ${event.value}`);

    if (event.value !== null) {
      this.startDate = new Date(event.value);
      this.loadingActivities = true;

      this.userService.getUsername(String(localStorage.getItem('token')))
        .subscribe((data) => {
          this.userData = data;

          this.userService.getUserActivities(this.userData.user, this.userData.token, this.datePipe.transform(this.startDate, 'yyyy-MM-dd'))
            .subscribe(data => {
              this.loadingActivities = false;
              this.userActivities = data;
            });
        });
    }
  }

  clickAlbums() {
    this.userService.getUsername(String(localStorage.getItem('token')))
      .subscribe((data) => {
        this.userData = data;
        this.loadingAlbums = true;

        this.userService.getUserAlbums(this.userData.user, this.userData.token)
          .subscribe(data => {
            this.loadingAlbums = false;
            this.userAlbums = data;
          });
      });
  }

  clickFiles() {
    this.userService.getUsername(String(localStorage.getItem('token')))
      .subscribe((data) => {
        this.userData = data;
        this.loadingFiles = true;

        this.userService.getUserFiles(this.userData.user, this.userData.token)
          .subscribe(data => {
            this.loadingFiles = false;
            this.userFiles = data;
          });
      });
  }

  clickProgressions() {
    this.userService.getUsername(String(localStorage.getItem('token')))
      .subscribe((data) => {
        this.userData = data;
        this.loadingProgressions = true;

        this.userService.getUserProgressions(this.userData.user, this.userData.token)
          .subscribe(data => {
            this.loadingProgressions = false;
            this.userProgressions = data;
          });
      });
  }

  clickScores() {
    this.userService.getUsername(String(localStorage.getItem('token')))
      .subscribe((data) => {
        this.userData = data;
        this.loadingScores = true;

        this.userService.getUserScores(this.userData.user, this.userData.token)
          .subscribe(data => {
            this.loadingScores = false;
            this.userScores = data;
          });
      });
  }

  clickTimelines() {
    this.userService.getUsername(String(localStorage.getItem('token')))
      .subscribe((data) => {
        this.userData = data;
        this.loadingTimelines = true;

        this.userService.getUserTimelines(this.userData.user, this.userData.token)
          .subscribe(data => {
            this.loadingTimelines = false;
            this.userTimelines = data;
          });
      });
  }

  getActivity(activityId: number) {
    this.userService.getUsername(String(localStorage.getItem('token')))
      .subscribe((data) => {
        this.userData = data;

        this.userService.getActivity(activityId, this.userData.token)
          .subscribe(data => this.activity = data);
      });
  }

  openDialog(slides: number, youtubeEmbedId: string, id: number) {
    this.dialog.open(UserDialog, {
      data: {
        slides: slides,
        youtubeEmbedId: youtubeEmbedId,
        id: id,
      },
      autoFocus: false,
    });
  }
}

@Component({
  selector: 'user.dialog',
  standalone: true,
	imports: [NgbCarouselModule, NgIf, NgFor],
  templateUrl: 'user.dialog.html',
  providers: [NgbCarouselConfig,SafePipe],
  styleUrls: ['./user.dialog.css'],
})
export class UserDialog {
  slides : number[] = [];
  youtubeURL : SafeResourceUrl = '';
  userId : number = 0;

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData, private safePipe: SafePipe) {
    for (let i = 1; i <= data.slides; i++) {
      this.slides.push(i); 
    }
    this.youtubeURL = this.safePipe.transform("https://www.youtube.com/embed/"+data.youtubeEmbedId+"?autoplay=1");
    this.userId = data.id;
  }
}
