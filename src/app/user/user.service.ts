import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpErrorHandler, HandleError } from '../http-error-handler.service';
import { environment } from '../../environments/environment';

import { Activity, ActivityApiResult } from '../activity/activity';
import { Album, AlbumApiResult } from '../album/album';
import { File, FileApiResult } from '../file/file';
import { Progression, ProgressionApiResult } from '../progression/progression';
import { Score, ScoreApiResult } from '../score/score';
import { Timeline, TimelineApiResult } from '../timeline/timeline';
import { User } from './user';
import { UserTokenSignin } from './userTokenSignin';

@Injectable()
export class UserService {
  activitiesUrl = environment.apiHost + '/activities';
  albumsUrl = environment.apiHost + '/albums';
  filesUrl = environment.apiHost + '/files';
  progressionsUrl = environment.apiHost + '/progressions';
  scoresUrl = environment.apiHost + '/user_has_scores';
  timelinesUrl = environment.apiHost + '/timelines';
  usersUrl = environment.apiHost + '/users/';
  tokenSignInUrl = environment.apiHost + '/tokensignin';
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('AlbumsService');
  }

  getActivity(activityId: number, token: string) {
    const url = this.activitiesUrl + '/' + activityId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Bearer ' + token
      })
    };

    return this.http.get<Activity>(url, httpOptions).pipe(
      catchError(this.handleError<Activity>(`getActivity activityId=${activityId}`))
    );
  }

  getUser(username: string, token: string): Observable<User> {
    const url = this.usersUrl + username;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Bearer ' + token
      })
    };

    return this.http.get<User>(url, httpOptions).pipe(
      catchError(this.handleError<User>(`getUser username=${username}`))
    );
  }

  getUserActivities(username: string, token: string, start: string | null): Observable<Activity[]> {
    const url = this.activitiesUrl + '?start[after]=' + start + '&groups[]=list&users.username=' + username;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Bearer ' + token
      })
    };

    return this.http.get<ActivityApiResult>(url, httpOptions).pipe(map(activityApiResult => activityApiResult['hydra:member']));
  }

  getUserAlbums(username: string, token: string): Observable<Album[]> {
    const url = this.albumsUrl + '?groups[]=list&users.username=' + username;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Bearer ' + token
      })
    };

    return this.http.get<AlbumApiResult>(url, httpOptions).pipe(map(albumApiResult => albumApiResult['hydra:member']));
  }

  getUserFiles(username: string, token: string): Observable<File[]> {
    const url = this.filesUrl + '?groups[]=list&users.username=' + username;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Bearer ' + token
      })
    };

    return this.http.get<FileApiResult>(url, httpOptions).pipe(map(fileApiResult => fileApiResult['hydra:member']));
  }

  getUserProgressions(username: string, token: string): Observable<Progression[]> {
    const url = this.progressionsUrl + '?groups[]=list&users.username=' + username;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Bearer ' + token
      })
    };

    return this.http.get<ProgressionApiResult>(url, httpOptions).pipe(map(progressionApiResult => progressionApiResult['hydra:member']));
  }

  getUserScores(username: string, token: string): Observable<Score[]> {
    const url = this.scoresUrl + '?groups[]=list&user.username=' + username;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Bearer ' + token
      })
    };

    return this.http.get<ScoreApiResult>(url, httpOptions).pipe(map(scoreApiResult => scoreApiResult['hydra:member']));
  }

  getUserTimelines(username: string, token: string): Observable<Timeline[]> {
    const url = this.timelinesUrl + '?groups[]=list&users.username=' + username;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Bearer ' + token
      })
    };

    return this.http.get<TimelineApiResult>(url, httpOptions).pipe(map(timelineApiResult => timelineApiResult['hydra:member']));
  }

  getUsername(token: string): Observable<UserTokenSignin> {
    const url = this.tokenSignInUrl;
    const body = new HttpParams()
    .set('idtoken', token);

    return this.http.post<UserTokenSignin>(url, body);
  }
}
