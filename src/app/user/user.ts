export interface User {
  id: number;
  name: string;
  surname: string;
  gamername: string;
  email: string;
  userKpis?: [{total: number, updatedAt: string, type: {slug: string}, kpiType: string, totalPoints: number, totalWins: number, totalDraws: number, totalLosses: number}];
  youtubeEmbedId: string;
  slides: number;
}
