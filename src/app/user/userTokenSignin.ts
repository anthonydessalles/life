export interface UserTokenSignin {
  user: string;
  token: string;
}
